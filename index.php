<?php

require_once 'autoload.php';
require_once 'head.php';
if (isset($_GET['user'])) {
    require_once 'userpage.php';
} else {
    require_once 'userlist.php';
}
require_once 'foot.php';
?>