<?php
/**
 * class extending db.php for handling various data
 */
class dbhandler extends db {

    function __construct() {
        parent::__construct();
    }

    function addUser($id) {
        $id = $this -> sanitize($id);
        $id = floor($id);
        if (is_numeric($id) && $id > 0) {
            //input is valid, now let's check whether such user exists
            $data = $this -> getJqueryDataByID($id);
            //if no such user exists, return fail message
            if (!$data)
                return "Šāds lietotājs neeksistē!";
            //check if user is already added
            $uname = $this -> fetchOne("SELECT username FROM users WHERE exsid='$id'");
            if ($this -> affected > 0)
                return "Šāds lietotājs jau eksistē sarakstā! (" . $uname['username'] . ")";

            //if this has passed, we are safe to go
            $date = date("Y-m-d H:i:s");
            $username = $data -> nick;
            $posts = $data -> posts;
            $karma = $data -> karma;
            $query = "INSERT INTO users (exsid, username, karma_added, posts_added, date_added, tmp_karma, tmp_posts) VALUES ('$id', '$username', '$karma', '$posts', '$date', '$karma', '$posts')";
            $this -> query($query);
            $this -> updateStats("users");
            return "Lietotājs pievienots!";
        } else {
            //invalid input
            return "Lietotāja ID ievadīts kļūdaini!";
        }
    }

    function getJqueryDataByID($id) {
        $url = "https://exs.lv/user_stats/json/" . $id;
        $json = file_get_contents($url);
        $data = json_decode($json);
        return $data;
    }

    function populateTable() {
        $result = $this -> query("SELECT id,username,date_added FROM users");
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td><a href='user/" . $row['id'] . "'>" . $row['username'] . "</a></td>";
            echo "<td>" . $row['date_added'] . "</td>";
            echo "</tr>";
        }
    }

    function updateStats($stat) {
        if ($stat === "date") {
            $date = date("Y-m-d H:i:s");
            $this -> query("UPDATE stats SET last_update = '$date'");
        } else {
            $stat = $this -> sanitize($stat);
            $this -> query("UPDATE stats SET $stat = $stat + 1");
        }
    }

    function getUserByID($id, $populate) {
        $id = $this -> sanitize($id);
        $userResult = $this -> fetchOne("SELECT * FROM users WHERE id='$id'");
        if ($this -> affected === 0)
            return false;
        $karmaSrc = "includes/graph.php?karma&id=" . $id;
        $postsSrc = "includes/graph.php?posts&id=" . $id;
        if ($populate) {
            $this -> populateUserPage($this -> getJqueryDataByID($userResult['exsid']), $userResult, $karmaSrc, $postsSrc, $id);
            return true;
        } else {
            return $result;
        }
    }

    function dailySave() {
        $result = $this -> query("SELECT id,karma_day,posts_day FROM users");
        while ($row = mysqli_fetch_assoc($result)) {
            $statement = $this -> _connection -> prepare("INSERT INTO daily_stats (userid, karma, posts, date) VALUES (?,?,?,?)");
            $statement -> bind_param("iiis", $id, $karma, $posts, $date);
            $id = $row['id'];
            $karma = $row['karma_day'];
            $posts = $row['posts_day'];
            $date = new DateTime(date("Y-m-d"));
            //subtract one day, because the script executes at 00:00 which is technically the next day already
            $date -> sub(new DateInterval("P1D"));
            $date = $date -> format("Y-m-d");
            $statement -> execute();
            $statement -> close();
        }
        return true;
    }

    function populateUserPage($jdata, $dbdata, $karmaSrc, $postsSrc, $id) {
        //days since inserting

        //check if there are at least 2 points to be displayed in the graph. We need at least 2 results (1 result contains one post and one karma data)
        $this -> query("SELECT id FROM daily_stats WHERE userid='$id'");
        if ($this -> affected > 1) {
            $karmaGraph = "<img src='" . $karmaSrc . "'/>";
            $postsGraph = "<img src='" . $postsSrc . "'/>";
        } else {
            $karmaGraph = "<p>Lai attēlotu grafiku, ir nepieciešami vismaz 2 punkti, tātad jāuzgaida vismaz 2 dienas!</p>";
            $postsGraph = "<p>Lai attēlotu grafiku, ir nepieciešami vismaz 2 punkti, tātad jāuzgaida vismaz 2 dienas!</p>";
        }
        //find out for how many days the user has been added +1
        $days = date_diff(new DateTime(date("Y-m-d H:i:s")), new DateTime($dbdata['date_added']));
        $days = $days -> days + 1;
        echo "<div class='row userdata'>
                <div class='small-12 medium-12 large-12 columns'>
                    <div class='panel radius clearfix'>
                        <img class='left' src='" . $jdata -> avatar -> medium . "' />
                        <h5><a href='https://exs.lv/user/" . $dbdata['exsid'] . "'>" . $dbdata['username'] . "</a></h5>
                        <p>Sarakstam pievienots: " . $dbdata['date_added'] . "</p>
                    </div>
                </div>
            </div>";

        echo "<div class='row'>";

        echo "<div class='small-12 medium-12 large-6 columns'>
                <div class='panel callout radius'>
                    <h5>Karma</h5>
                    <hr />
                    <p>
                        Karma pievienošanas brīdī: " . $dbdata['karma_added'] . "
                    </p>
                    <p>
                        Karma šodien: +" . $dbdata['karma_day'] . "
                    </p>
                    <p>
                        Karma šonedēļ: +" . $dbdata['karma_week'] . "
                    </p>
                    <p>
                        Karma šomēnes: +" . $dbdata['karma_month'] . "
                    </p>
                    <p>
                        Karma tagad (stundas nobīde): " . $dbdata['tmp_karma'] . "
                    </p>
                    <p>
                        Karma tagad (reālā karma): " . $jdata -> karma . "
                    </p>
                    <p>
                        Karma vidēji diennaktī/mēnesī: " . round(($jdata -> karma - $dbdata['karma_added']) / $days, 2) . " / " . round(($jdata -> karma - $dbdata['karma_added']) / 30, 2) . "
                    </p>
                    " . $karmaGraph . "
                    <br/>
                    <span class='button radius small' id='karma_graph_data'>Grafika dati</span>
                    <table id='karma_graph_table'>
                    <thead>
                    <tr>
                        <th>Datums</th>
                        <th>Karma</th>
                    </tr>
                    </thead>
                    <tbody>";
        $result = $this -> query("SELECT date,karma FROM daily_stats WHERE userid='$id' ORDER BY date DESC LIMIT 7");
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row['date'] . "</td>";
            echo "<td>" . $row['karma'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>
        </table>
                </div>
            </div>";

        echo "<div class='small-12 medium-12 large-6 columns'>
                <div class='panel callout radius'>
                    <h5>Posti</h5>
                    <hr />
                    <p>
                        Posti pievienošanas brīdī: " . $dbdata['posts_added'] . "
                    </p>
                    <p>
                        Posti šodien: +" . $dbdata['posts_day'] . "
                    </p>
                    <p>
                        Posti šonedēļ: +" . $dbdata['posts_week'] . "
                    </p>
                    <p>
                        Posti šomēnes: +" . $dbdata['posts_month'] . "
                    </p>
                    <p>
                        Posti tagad (stundas nobīde): " . $dbdata['tmp_posts'] . "
                    </p>
                    <p>
                        Posti tagad (reālie posti): " . $jdata -> posts . "
                    </p>
                    <p>
                        Posti vidēji diennaktī/mēnesī: " . round(($jdata -> posts - $dbdata['posts_added']) / $days, 2) . " / " . round(($jdata -> posts - $dbdata['posts_added']) / 30, 2) . "
                    </p>
                    " . $postsGraph . "
                    <br/>
                    <span class='button radius small' id='post_graph_data'>Grafika dati</span>
                    <table id='post_graph_table'>
                    <thead>
                    <tr>
                        <th>Datums</th>
                        <th>Posti</th>
                    </tr>
                    </thead>
                    <tbody>";
        $result = $this -> query("SELECT date,posts FROM daily_stats WHERE userid='$id' ORDER BY date DESC LIMIT 7");
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row['date'] . "</td>";
            echo "<td>" . $row['posts'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>
        </table>
                </div>
            </div>";

        echo "</div>";
    }

}
