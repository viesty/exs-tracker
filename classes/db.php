<?php

/**
 * Main DB class. Database credentials taken from config.php
 */
class db {
    public $affected = 0;
    public $lastQuery = 'no last query';
    public $_connection;

    function __construct() {
        $this -> connect();
        ob_start();
    }

    function connect() {
        $this -> _connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB);
        if (!$this -> _connection) {
            die(print_r(mysqli_connect_error()));
        }
        mysqli_set_charset($this -> _connection, "utf-8");
    }

    function sanitize($string) {//method for escaping query input
        $string = stripslashes($string);
        $string = mysqli_real_escape_string($this -> _connection, $string);
        return $string;
    }

    function query($query) {
        $this -> lastQuery = $query;
        if (!$result = mysqli_query($this -> _connection, $query)) {
            mysqli_error($this -> _connection);
        } else {
            $this -> affected = mysqli_affected_rows($this -> _connection);
        }
        return $result;
    }

    function fetchOne($query) {
        $result = $this -> query($query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    //call immediately after query, this will echo results and kill the script for examination.
    function analyzeQuery() {
        echo "Last query: " . $this -> lastQuery . "<br/>";
        echo "Affected rows: " . $this -> affected . "<br/>";
        die();
    }

    function close() {
        if (!mysqli_close($this -> _connection)) {
            die("Error closing the connection");
        }
    }

}
