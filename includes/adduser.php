<?php
//here we handle user adding to the database
$handler = new dbhandler();
$res = $handler->fetchOne("SELECT * FROM stats");
if (isset($_POST['adduser_button'])) {
    $_SESSION['response'] = $handler -> addUser($_POST['userid']);
    header("Location: index.php");
    die();
}
?>
<div class="row">
    <div class="small-12 medium-7 large-7 columns">
        <div class="row">
            <div class="panel"><p>Lietotāja ID ir cipari, no kuriem sastāv tava profila saite: https://exs.lv/user/<strong>xxxxxx</strong></p></div>
            <form action="" method="POST" accept-charset="utf-8">
                <div class="small-3 medium-3 large-3 columns">
                    <label for="userid" class="left inline">Lietotāja ID: </label>
                </div>
                <div class="small-9 medium-9 large-9 columns">
                    <input type="text" name="userid" id="userid"/>
                </div>
        </div>
        
        <?php
        //if there was an error while adding user, show it. 
        if (isset($_SESSION['response'])) {
        ?>
        <div class="row">
            <div class="small-12 medium-12 large-12 columns">
                <div class="alert-box warning radius">
                    <?php echo $_SESSION['response']; ?>
                </div>
            </div>
        </div>
        <?php
            unset($_SESSION['response']);
        }
        ?>
        
        <div class="row" id="adduser">
            <div class="small-12 medium-12 large-12 columns">
                <input type="submit" name="adduser_button" value="Pievienot" class="button expand"/>
            </div>
            </form>
        </div>
    </div>
    <div class="small-12 medium-5 large-5 columns">
        <div class="row">
            <div class="small-12 medium-12 large-12 columns">
                <div class="panel closetext">
                    <h5>Statistika</h5>
                    <p>
                        Dati pēdējoreiz atjaunoti: <?php echo $res['last_update']; ?>
                    </p>
                    <p>
                        Kopā pievienoti lietotāji: <?php echo $res['users']; ?>
                    </p>
                    <p>
                        Dati kopā atjaunoti <?php echo $res['updates']; ?> reizes.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>