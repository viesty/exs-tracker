<?php
require_once '../autoload.php';
$db = new db();
require_once ('jpgraph.php');
require_once ('jpgraph_line.php');
//set up the graph
$graph = new Graph(430, 400);
$graph -> SetScale("textlin");
$theme_class = new UniversalTheme;
$graph -> SetTheme($theme_class);
$graph -> img -> SetAntiAliasing(false);
$graph -> title -> Set('Nedēļas statistika');
$graph -> SetBox(false);
$graph -> img -> SetAntiAliasing();
$graph -> yaxis -> HideZeroLabel();
$graph -> yaxis -> HideLine(false);
$graph -> yaxis -> HideTicks(false, false);
$graph -> xgrid -> Show();
$graph -> xgrid -> SetLineStyle("solid");
$karmaLabels = [];
$karmaData = [];
$postLabels = [];
$postData = [];
//do we want karma graph?
if (isset($_GET['karma'])) {
    $id = $db -> sanitize($_GET['id']);
    $karmaResult = $db -> query("SELECT karma,date FROM daily_stats WHERE userid='$id' ORDER BY date DESC LIMIT 7 ");

    while ($row = mysqli_fetch_assoc($karmaResult)) {
        array_push($karmaLabels, $row['date']);
        array_push($karmaData, $row['karma']);
    }
    //prepare labels. reverse the label array because we want oldest labels on the left side of X axis
    $graph -> xaxis -> SetTickLabels(array_reverse($karmaLabels));
    //so we have to reverse data arrays too
    $karmaData = array_reverse($karmaData);
    //shift labels in a 50 degree angle so that dates don't overlap
    $graph -> xaxis -> SetLabelAngle(50);
    $p1 = new LinePlot($karmaData);
    
} elseif (isset($_GET['posts'])) {
    $id = $db -> sanitize($_GET['id']);
    $postsResult = $db -> query("SELECT posts,date FROM daily_stats WHERE userid='$id' ORDER BY date DESC LIMIT 7");
    while ($row = mysqli_fetch_assoc($postsResult)) {
        array_push($postLabels, $row['date']);
        array_push($postData, $row['posts']);
    }
    $graph -> xaxis -> SetTickLabels(array_reverse($postLabels));
    $postData = array_reverse($postData);
    $graph -> xaxis -> SetLabelAngle(50);
    $p1 = new LinePlot($postData);
}
$graph -> xgrid -> SetColor('#E3E3E3');

$graph -> Add($p1);
$p1 -> SetColor("#6495ED");
//draw the graph
$graph -> Stroke();
