<!doctype html>
<html class="no-js" lang="en">
    <head>
        <BASE href="/tracker/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Exs lietotāju aktivitātes izsekošana</title>
        <link rel="stylesheet" href="styles/foundation.min.css" />
        <link rel="stylesheet" href="styles/main.css" />
        <script src="js/modernizr.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/foundation.min.js"></script>
    </head>
    <body>
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="index.php">Exs lietotāju statistika</a></h1>
                </li>
            </ul>
        </nav>
