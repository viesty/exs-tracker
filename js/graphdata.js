$(document).ready(function() {
	$('#post_graph_table').hide();
	$('#karma_graph_table').hide();
	$('#post_graph_data').click(function() {
		$('#post_graph_table').toggle();
	});
	$('#karma_graph_data').click(function() {
		$('#karma_graph_table').toggle();
	});
});
