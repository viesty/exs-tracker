<?php
require_once "../autoload.php";
//launch this file every hour as a cronjob

//launch this file with GET parameter of ?resetDay every day at 00:00
//launch this file with GET parameter of ?resetWeek every mondayday at 00:00
//launch this file with GET parameter of ?resetMonth every first day of month at 00:00


//go through all the users in the DB
$handler = new dbhandler();
$db = new db();

//check for reset parameters
$devs = ['127.0.0.1', '::1', 'localhost'];
if (in_array($_SERVER['REMOTE_ADDR'], $devs)) {
    if (isset($_GET['resetDay'])) {
        //save day's data of each user.
        if($handler->dailySave()){
            $db->query("UPDATE users SET karma_day = 0");
            $db->query("UPDATE users SET posts_day = 0");  
        }
    }
    if (isset($_GET['resetWeek'])) {
        $db->query("UPDATE users SET karma_week = 0");
        $db->query("UPDATE users SET posts_week = 0");
    }
    if (isset($_GET['resetMonth'])) {
        $db->query("UPDATE users SET karma_month = 0");
        $db->query("UPDATE users SET posts_month = 0");
    }
}

$result = $handler -> query("SELECT * FROM users");
while ($row = mysqli_fetch_assoc($result)) {
    //load their json files
    $jdata = $handler -> getJqueryDataByID($row['exsid']);
    //update tmp_karma and tmp_posts to current values
    //prepare statement for user stat insertion
    $query = $db -> _connection -> prepare("UPDATE users SET tmp_karma=?, tmp_posts=?, karma_day=?, karma_week=?, karma_month=?, 
    posts_day=?, posts_week=?, posts_month=? WHERE id='{$row['id']}'");
    //parameters for query. All integers
    $query -> bind_param("iiiiiiii", $karma, $posts, $k_day, $k_week, $k_month, $p_day, $p_week, $p_month);
    //calculate the parameters
    //based on the tmp_* change, calculate values for day, week, month
    $karma = $jdata -> karma;
    $posts = $jdata -> posts;
    $karmachange = $karma - $row['tmp_karma'];
    $postchange = $posts - $row['tmp_posts'];
    $k_day = $row['karma_day'] + $karmachange;
    $k_week = $row['karma_week'] + $karmachange;
    $k_month = $row['karma_month'] + $karmachange;
    $p_day = $row['posts_day'] + $postchange;
    $p_week = $row['posts_week'] + $postchange;
    $p_month = $row['posts_month'] + $postchange;
    $query -> execute();
    $query -> close();
}

$handler -> updateStats("updates");
$handler -> updateStats("date");
die();
