<?php
require_once 'adduser.php';
$handler = new dbhandler();
?>

<hr />

<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <table>
            <caption>
                Nospiežot uz lietotājvārda, tiks atvērta konkrētā lietotāja statistikas lapa.
            </caption>
            <thead>
                <tr>
                    <th width="500">Lietotājvārds</th>
                    <th width="500">Kad pievienots?</th>
                </tr>
            </thead>
            <tbody>
                <?php $handler -> populateTable(); ?>
            </tbody>
        </table>
    </div>
</div>